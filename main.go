package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"

	//"strings"

	//"strconv"

	//"strconv"
	//"strings"
	"time"
)

type Transaction struct {
	OrderId, Quantity, TotalPrice int
	ProductName                   string
}

type TransactionSlice []Transaction

func OrderIn(channel chan<- Transaction, data Transaction) {
	time.Sleep(2 * time.Second)
	channel <- data
}

// func OnlyOut(channel <-chan Transaction)  {
// 	var data TransactionSlice
// 	data = append(data, channel)

// 	fmt.Println(data)
// }

func main() {
	channel := make(chan Transaction)
	defer close(channel)

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Create Order")

	for i := 0; i < 2; i++ {
		var transaction Transaction

		fmt.Print("Order Id: ")
		orderId, _, _ := reader.ReadLine()
		transaction.OrderId, _ = strconv.Atoi(string(orderId))

		fmt.Print("Product Name: ")
		productName, _, _ := reader.ReadLine()
		transaction.ProductName = string(productName)

		fmt.Print("Quantity: ")
		quantity, _, _ := reader.ReadLine()
		transaction.Quantity, _ = strconv.Atoi(string(quantity))

		fmt.Print("TotalPrice: ")
		totalPrice, _, _ := reader.ReadLine()
		transaction.TotalPrice, _ = strconv.Atoi(string(totalPrice))

		go OrderIn(channel, transaction)
	}

	// var transaction Transaction
	// transaction.OrderId = "001"

	// channel := make(chan Transaction)

	// defer close(channel)

	// go OrderIn(channel, transaction)

	// data := <-channel
	// fmt.Println(data)

	// time.Sleep(5 * time.Second)
}
